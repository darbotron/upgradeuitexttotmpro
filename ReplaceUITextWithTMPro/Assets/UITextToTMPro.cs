﻿using System.Collections.Generic;
using System.Reflection;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class TextToTextMeshPro : EditorWindow
{
	[System.Serializable]
	public class ReferringObjectInfo
	{
		public MonoBehaviour		m_component		= null;
		public List< FieldInfo >	m_listOfFields	= null;
	}

	// used to store info about the selection and keep track of the UI.text objects and their references
	[System.Serializable]
	public class UITextToTMProTextUpgradeInfo
	{
		public GameObject					m_gameObject				= null;
		public UnityEngine.UI.Text			m_uiText					= null;
		public List< ReferringObjectInfo >	m_listOfReferringComponents	= null;
	}

	// serialiseable properties survive editor dll reloads
	[SerializeField] Vector2								m_scrollPosition			= Vector2.zero;
	[SerializeField] List< UITextToTMProTextUpgradeInfo >	m_listOfUiTextUpgradeInfo	= null;


	[MenuItem( "Tools/UI.Text -> TMPro UGUI Text upgrader" )]
	public static void ShowWindow()
	{
		EditorWindow.GetWindow< TextToTextMeshPro >( true, "Upgrade UI.Text to TMPro.UGui Text", true );
	}

	private void OnGUI()
	{
		if( TMPro.TMP_Settings.defaultFontAsset == null )
		{
			EditorUtility.DisplayDialog( "ERROR!", "Assign a default font asset in project settings!", "OK" );
			return;
		}

		if( null == m_listOfUiTextUpgradeInfo )
		{
			if( Selection.gameObjects.Length == 0 )
			{
				EditorGUILayout.LabelField( "You need to select an object in the hierarchy!" );
				return;			
			}

			m_listOfUiTextUpgradeInfo = new List< UITextToTMProTextUpgradeInfo >();

			foreach( var selectedObj in Selection.gameObjects )
			{
				foreach( var uiText in selectedObj.GetComponentsInChildren< UnityEngine.UI.Text >( true ) )
				{
					m_listOfUiTextUpgradeInfo.Add( new UITextToTMProTextUpgradeInfo(){ m_uiText = uiText, m_gameObject = uiText.gameObject, m_listOfReferringComponents = null } );
				}
			}
		}

		if( GUILayout.Button( "Refresh UI from current selection" ) )
		{
			 m_listOfUiTextUpgradeInfo = null;
		}

		EditorGUILayout.Separator();
		EditorGUILayout.Separator();

		if(	null != m_listOfUiTextUpgradeInfo )
		{
			EditorGUILayout.LabelField( $"{m_listOfUiTextUpgradeInfo.Count} instances of UI.Text selected (or children of selected)" );

			using( var scrollGroup = new EditorGUILayout.ScrollViewScope( m_scrollPosition ) )
			{
				m_scrollPosition = scrollGroup.scrollPosition;

				foreach( var upgradeInfo in m_listOfUiTextUpgradeInfo )
				{
					using( new EditorGUILayout.VerticalScope( EditorStyles.helpBox ) )
					{
						using( new EditorGUILayout.HorizontalScope() )
						{
							if( GUILayout.Button( upgradeInfo.m_gameObject.NameWithFullScenePath() ) )
							{
								Selection.activeObject = upgradeInfo.m_gameObject;
							}

							if( GUILayout.Button( "refresh references" ) )
							{
								upgradeInfo.m_listOfReferringComponents = null;
							}

							if( null == upgradeInfo.m_listOfReferringComponents )
							{
								upgradeInfo.m_listOfReferringComponents = new List< ReferringObjectInfo >();

								// this is the bit which finds all the references that will need updating
								FindAllMonobehavioursReferencingConponentInstanceInScene( upgradeInfo.m_uiText, upgradeInfo.m_listOfReferringComponents );
							}
						}

						using( new EditorGUILayout.HorizontalScope() )
						{
							EditorGUILayout.Space();
							using( new EditorGUILayout.VerticalScope() )
							{
								EditorGUILayout.LabelField( $"Found {upgradeInfo.m_listOfReferringComponents.Count} references:" );

								if( upgradeInfo.m_listOfReferringComponents.Count > 0 )
								{
									foreach( var referringComponentInfo in upgradeInfo.m_listOfReferringComponents )
									{
										using( new EditorGUILayout.HorizontalScope() )
										{
											GUILayout.Label( $"UI.Text ref: {referringComponentInfo.m_component.NameWithFullScenePath()}" );
											if( GUILayout.Button( "[ select ]" ) )
											{
												Selection.activeObject = referringComponentInfo.m_component;
											}
										}
										foreach( var finfo in referringComponentInfo.m_listOfFields )
										{													   
											EditorGUILayout.LabelField( $"--> {referringComponentInfo.m_component.GetType().Name}.{finfo.Name}" );
										}
									}
								}
							}
						}

						EditorGUILayout.Space();
						EditorGUILayout.Space();
					}
				}
			}
		}
	}


	static List< MonoBehaviour > sm_tempMnBhvrList = new List<MonoBehaviour>(); // NOT THREADSAFE!

	/// <summary>
	/// this function uses reflection to find any private or public fields which have the type of the component passed as component
	/// (this could be any MonoBehvaiour, but it's only ever UI.Text in this code right now...)
	/// </summary>
	void FindAllMonobehavioursReferencingConponentInstanceInScene< T >( T componentToFindReferencesOf, List< ReferringObjectInfo > listToFill ) where T : MonoBehaviour
	{
		GameObject[] sceneRootObjectArray = componentToFindReferencesOf.gameObject.scene.GetRootGameObjects();

		foreach( var rootObject in sceneRootObjectArray )
		{
			rootObject.GetComponentsInChildren( true, sm_tempMnBhvrList ); // is cleared with each call   

			foreach( var mnBhvr in sm_tempMnBhvrList )
			{
				// reflection is cooooool
				var tidMnBhvr	= mnBhvr.GetType();
				var finfoArray	= tidMnBhvr	.GetFields	( BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic )
											.Where		( 
															( finfo ) => 
															{ 
																if( finfo.FieldType == typeof( T ) )
																{
																	var fieldValue = finfo.GetValue( mnBhvr ) as T;
																	if( fieldValue == componentToFindReferencesOf ) 
																	{
																		return true;
																	}
																}
																return false;
															} 
														)
											.ToArray();
				
				if( finfoArray?.Length > 0 )
				{
					listToFill.Add( new ReferringObjectInfo(){ m_component = mnBhvr, m_listOfFields = new List<FieldInfo>( finfoArray ) } );
				}
			}
		}
	}

	/// <summary>
	/// what follows is code I got off the internet from here: https://forum.unity.com/threads/replacing-text-with-textmesh-pro.515594/#post-4854512
	/// I checked it and it seems to work ok, but it only does a simple swap right now, need to integrater this into the above stuff so 
	/// all the references to the UI.Text instance are handled when we replace the UI.Text with TMPro UGUI Text
	/// </summary>
	public class TextMeshProSettings
	{
		public bool Enabled;
		public FontStyles FontStyle;
		public float FontSize;
		public float FontSizeMin;
		public float FontSizeMax;
		public float LineSpacing;
		public bool EnableRichText;
		public bool EnableAutoSizing;
		public TextAlignmentOptions TextAlignmentOptions;
		public bool WrappingEnabled;
		public TextOverflowModes TextOverflowModes;
		public string Text;
		public Color Color;
		public bool RayCastTarget;
	}

	private static void ConvertTextToTextMeshPro( GameObject target )
	{
		var settings = GetTextMeshProSettings( target );

		DestroyImmediate( target.GetComponent<Text>() );

		var tmp = target.AddComponent<TextMeshProUGUI>();
		tmp.enabled = settings.Enabled;
		tmp.fontStyle = settings.FontStyle;
		tmp.fontSize = settings.FontSize;
		tmp.fontSizeMin = settings.FontSizeMin;
		tmp.fontSizeMax = settings.FontSizeMax;
		tmp.lineSpacing = settings.LineSpacing;
		tmp.richText = settings.EnableRichText;
		tmp.enableAutoSizing = settings.EnableAutoSizing;
		tmp.alignment = settings.TextAlignmentOptions;
		tmp.enableWordWrapping = settings.WrappingEnabled;
		tmp.overflowMode = settings.TextOverflowModes;
		tmp.text = settings.Text;
		tmp.color = settings.Color;
		tmp.raycastTarget = settings.RayCastTarget;
	}

	private static TextMeshProSettings GetTextMeshProSettings( GameObject gameObject )
	{
		var uiText = gameObject.GetComponent<Text>();
		if( uiText == null )
		{
			EditorUtility.DisplayDialog( "ERROR!", "You must select a Unity UI Text Object to convert.", "OK", "" );
			return null;
		}

		return new TextMeshProSettings
		{
			Enabled = uiText.enabled,
			FontStyle = FontStyleToFontStyles( uiText.fontStyle ),
			FontSize = uiText.fontSize,
			FontSizeMin = uiText.resizeTextMinSize,
			FontSizeMax = uiText.resizeTextMaxSize,
			LineSpacing = uiText.lineSpacing,
			EnableRichText = uiText.supportRichText,
			EnableAutoSizing = uiText.resizeTextForBestFit,
			TextAlignmentOptions = TextAnchorToTextAlignmentOptions( uiText.alignment ),
			WrappingEnabled = HorizontalWrapModeToBool( uiText.horizontalOverflow ),
			TextOverflowModes = VerticalWrapModeToTextOverflowModes( uiText.verticalOverflow ),
			Text = uiText.text,
			Color = uiText.color,
			RayCastTarget = uiText.raycastTarget
		};
	}

	private static bool HorizontalWrapModeToBool( HorizontalWrapMode overflow )
	{
		return overflow == HorizontalWrapMode.Wrap;
	}

	private static TextOverflowModes VerticalWrapModeToTextOverflowModes( VerticalWrapMode verticalOverflow )
	{
		return verticalOverflow == VerticalWrapMode.Truncate ? TextOverflowModes.Truncate : TextOverflowModes.Overflow;
	}

	private static FontStyles FontStyleToFontStyles( FontStyle fontStyle )
	{
		switch( fontStyle )
		{
		case FontStyle.Normal:
			return FontStyles.Normal;

		case FontStyle.Bold:
			return FontStyles.Bold;

		case FontStyle.Italic:
			return FontStyles.Italic;

		case FontStyle.BoldAndItalic:
			return FontStyles.Bold | FontStyles.Italic;
		}

		Debug.LogWarning( "Unhandled font style " + fontStyle );
		return FontStyles.Normal;
	}

	private static TextAlignmentOptions TextAnchorToTextAlignmentOptions( TextAnchor textAnchor )
	{
		switch( textAnchor )
		{
		case TextAnchor.UpperLeft:
			return TextAlignmentOptions.TopLeft;

		case TextAnchor.UpperCenter:
			return TextAlignmentOptions.Top;

		case TextAnchor.UpperRight:
			return TextAlignmentOptions.TopRight;

		case TextAnchor.MiddleLeft:
			return TextAlignmentOptions.Left;

		case TextAnchor.MiddleCenter:
			return TextAlignmentOptions.Center;

		case TextAnchor.MiddleRight:
			return TextAlignmentOptions.Right;

		case TextAnchor.LowerLeft:
			return TextAlignmentOptions.BottomLeft;

		case TextAnchor.LowerCenter:
			return TextAlignmentOptions.Bottom;

		case TextAnchor.LowerRight:
			return TextAlignmentOptions.BottomRight;
		}

		Debug.LogWarning( "Unhandled text anchor " + textAnchor );
		return TextAlignmentOptions.TopLeft;
	}
}

/// <summary>
/// handy helpers to get the full scene name of either a game object / the game obejct a component is attehed to
/// </summary>
public static class HelperExtensionMethods
{
	static System.Text.StringBuilder sm_tempStringBuilder = new System.Text.StringBuilder( 512 ); // NOT THREADSAFE!

	public static string NameWithFullScenePath( this GameObject gameObj )
	{
		RecursiveConcatenateScenePath( gameObj.transform, sm_tempStringBuilder );
		return sm_tempStringBuilder.ToString();
	}

	public static string NameWithFullScenePath< T >( this T component ) where T: Component
	{
		return NameWithFullScenePath( component.gameObject );
	}

	/// <summary>
	/// recursive always feels easiest for this sort of stuff - it's a bit of a brainwrong if you're not used to the code idiom though
	/// </summary>
	private static void RecursiveConcatenateScenePath( Transform transform, System.Text.StringBuilder stringBuilder )
	{
		if( null == transform.parent )
		{
			stringBuilder.Clear();
		}
		else
		{
			RecursiveConcatenateScenePath( transform.parent, stringBuilder );
		}
		stringBuilder.AppendFormat( "/{0}", transform.name );
	}
}