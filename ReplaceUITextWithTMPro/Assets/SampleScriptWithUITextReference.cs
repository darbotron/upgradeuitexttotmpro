﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleScriptWithUITextReference : MonoBehaviour
{
	public UnityEngine.UI.Text						m_uiTextReference		= null;
	[SerializeField] private UnityEngine.UI.Text	m_uiTextReferenceTwo	= null;

    // Start is called before the first frame update
    void Start()
    {
		m_uiTextReference.text		= "set at runtime";
		m_uiTextReferenceTwo.text	= "also set at runtime";
   }

    // Update is called once per frame
    void Update()
    {}
}
