# Overview
*Unfinished* 
Proof of concept Unity Editor Window for upgrading UI.Text to Text Mesh Pro I wrote in case it was helpful to a friend.

This represents a "0th pass" solution for most of the parts of the process which çan be automated 

(note: it is necessary to manually edit code to do this upgrade; it may also require editing of Unity 
animations and all sort of other knock ons so be warned this is potentially a non-trivial thing to do)

## License
All the portions of this code which I wrote are covered by "The Unlicense" : https://choosealicense.com/licenses/unlicense/
(TL;DR: do what you like with it, don't blame me if it doesn't work how you expect / at all)

## State of this right now (2020/07/19)

* there's an editor window under "Tools/UI.Text -> TMPro UGUI Text upgrader"
* there's a sample scene (main.unity) set up for me to run the editor window onto test it
* you can make a selection in the hierarchy and it'll find all the child instances of UI.Text...
* ...and for each a list of MonoBehaviours in the scene which refer to them
* all the objects found can be selected in the hierarchy view by clicking on buttons in the editor window

There is already code which can replace a UI.Text with a TMPRoUGUIText and copy the settings across from 
one to the other sensibly, I've tested it but it's not currently called 
(code for this borrowed from here: https://forum.unity.com/threads/replacing-text-with-textmesh-pro.515594/#post-4854512)


## To get this working
To get this to work you'll probably be best to do something like:

1) manually add new text mesh pro editor variables in all the places you have UI.Text 
editor variables right now (or write a regexp to do it maybe?) 

2) I would make the name of the new variable be the same as the old one with maybe 
"tmp" on the end or something so you can match the names by string in the auto replacer tool later (not yet working!)

3) at this point you would run the replacer tool (not yet functioning!!) which would remove all 
the UI.Text components and replace them with tmpro equivalents, and would set all the editor variables 
so the ones which match by name (from step 2) now point at the new TMPro 
text objects on the same GameObjects that the original UI.text objects were on

3a) The tool should validate that all the monobehaviours with existing references to each UI.Text 
have a matching empty TMPro UGUI Text reference before it starts (it doesn't do this yet).

4) now you can just go through and manually remove all the references to UI.Text and use search & replace 
in each C# script to point all the code at the new TMPro references you made. 
The TMpro text monobehaviour has almost entirely the same fields / properties etc. \as the UI.Text, 
just capitalised differently IIRC so should be pretty painless.

5) Unity in-editor animations *should* be pretty simple to fix up automatically for the same reason; 
I've tested it and the below works fine doing search and replace in the animation in this project in a text editor :)

6) also this unity package could be cannibalised to help extend this to a whole project rather than just a selection https://github.com/yasirkula/UnityAssetUsageDetector

###GUIDs for search & replace in animations

To fix up any Unity animations which were using UI.Text to work with the new TMPro.UGUI Text components 
you can use a text editor with search and replace. I've tested this (there's a single anim in the project) 
and it works fine...

UI.Text GUID line - rplace this with the one below for Text Mesh Pro UGUI:
`script: {fileID: 708705254, guid: f70555f144d8491a825f0804e09c671c, type: 3}`

Text Mesh Pro UGUI:
`script: {fileID: 11500000, guid: f4688fdb7df04437aeb418b961361dc5, type: 3}`


Individual curves have different names between the two classes because the member variables are differently named. 
Replace the member name in the 1st column with the menber name in the 2nd (his is just font colour but I'll add to it 
as / when / if this progresses):

| UI.Text 	| TMPro 	|
|---------	|-------	|
|m_Color	| m_fontColor	|
